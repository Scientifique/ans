
#include "Viewpoint.h"

double Viewpoint::scaleInterval = 0.1;

Viewpoint::Viewpoint() : pos{}, scale{ 1.0 }, minScale{ 1.0 }, maxScale{ 10.0 }
{
}

bool Viewpoint::Scale(double s) {
	
	this->scale *= s;
	
	if (this->scale > this->maxScale) {
		
		this->scale = this->maxScale;
		return false;

	} else if (this->scale < this->minScale) {
		
		this->scale = this->minScale;
		return false;

	} else {
		return true;
	}
}

double Viewpoint::getScale() {
	return this->scale;
}

bool Viewpoint::ScaleUp() {

	double factor = 1.0 + scaleInterval;
	return this->Scale(factor);
}

bool Viewpoint::ScaleDown() {

	double factor = 1.0 - scaleInterval;
	return this->Scale(factor);
}
