#pragma once
#include "Point.h";

class Viewpoint{

private:

	Point pos;
	double scale;
    double minScale;
	double maxScale;

public:

	Viewpoint();
	static double scaleInterval;
	bool Scale(double s);
	bool ScaleUp();
	bool ScaleDown();
	double getScale();
};


