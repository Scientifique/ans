#pragma once

#include <QtGui>
#include <qgraphicsview.h>
#include <qgraphicsscene.h>
#include "Hexagon.h"
#include "Antenna.h"
#include "hive.h"

class PaintWidget : public QGraphicsView
{

private:
	Hive current;
	std::vector<Antenna> network;
	QGraphicsScene *myScene;

public:
	PaintWidget();
	~PaintWidget();

	virtual void paintEvent(QPaintEvent *event);
	void drawHexa(const Hexagon& hex, QGraphicsScene *gs);
	void drawHive();
};

