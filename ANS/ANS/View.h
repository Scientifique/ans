#pragma once

#include <qgraphicsview.h>
#include <QWheelEvent>
#include <Qmenu>
#include <QMessageBox>
#include <QDialog>
#include <qfiledialog.h>
#include <QDebug>

#include "AntenneDialog.h"


#include <Qstring>

class View : public QGraphicsView
{
	Q_OBJECT

private:
	int m_originX;
	int m_originY;

	QAction *AddAnt;
	QAction *AddMap;
	QMenu *menu;

public:
	std::vector<Antenna*> network;

	View();
	~View();
	void setAntennaNetwork(std::vector<Antenna*> *network);

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);

protected Q_SLOTS:
	
	void AjouterAntenne();
	void wheelEvent(QWheelEvent *event);
	void AjouterCarte();
};

