#include "ANS.h"
#include "qpainter.h"
#include "qgridlayout.h"

ANS::ANS(QWidget *parent)
	: QMainWindow(parent), current{ 40,23,{50,50} }
{
	ui.setupUi(this);

	view = new View();
	myScene = new QGraphicsScene();
	ui.horizontalLayout->addWidget(view);

	updateHexagons();
	redrawView();
}

ANS::~ANS() {
	delete myScene;
	delete view;
}

void ANS::updateHexagons() {

	std::vector<Color> tc;

	for (int a = 0; a < view->network.size(); a++) {

		tc.push_back(Color(view->network.at(a)->GetColor()));
	}

	for (int i = 0; i < current.MatHex.size(); i++) {

		for (int j = 0; j < current.MatHex.at(0).size(); j++) {

			for (int a = 0; a < view->network.size(); a++) {

				double signal = view->network.at(a)->Signal(view->network.at(a)->Position().Distance(current.getMatHex().at(i).at(j).Position()));
				int alpha = Antenna::ToTransparency(Antenna::GetInBounds(signal));
				tc.at(a).setAlpha(alpha);
			}

			current.MatHex.at(i).at(j).setColor(Color::Synthesize(tc));
		}
	}
}

void ANS::redrawView() {

	//QPointF pos = view->

	for (int a = 0; a < view->network.size(); a++) {
		
		myScene->removeItem(view->network[a]);
	}
	//delete myScene;

	myScene->clear();// = new QGraphicsScene();

	drawHive();
	drawAntennas();

	view->setScene(myScene);
	view->show();
}

void ANS::refresh() {

	updateHexagons();
	redrawView();
}

void ANS::drawHexa(const Hexagon& hex) {

	QPolygon poly = QPolygon(Point::Convert(hex.getVertices()));

	Color c = hex.getColor();
	c.setAlpha(c.Alpha() / 4.5 + 64);

	QBrush brush = QBrush(QColor(c.R(), c.G(), c.B(), c.Alpha()));
	QPen pen = QPen(QBrush(QColor(100,100,100,200)), 1.0);

	myScene->addPolygon(poly, pen, brush);
}

void ANS::drawHive() {

	for (int i = 0; i < current.getMatHex().size(); i++) {

		for (int j = 0; j < current.getMatHex().at(0).size(); j++) {

			drawHexa(current.getMatHex().at(i).at(j));
		}
	}
}

void ANS::drawAntennas() {

	for (int i = 0; i < view->network.size(); i++) {

		myScene->addItem(view->network[i]);
	}
}

void ANS::mousePressEvent(QMouseEvent *event) {

	if (event->button() == Qt::MiddleButton) {


		updateHexagons();
		redrawView();

	} else {

		QMainWindow::mousePressEvent(event);
	}
}
