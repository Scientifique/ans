#include "Hive.h"
#include <math.h>

Hive::Hive(int heightH, const int widhtH, Point point)
{
	int x = point.X();
	int y = point.Y();
	int s = y;
	int z = x;

	double d = Hexagon::size;
	d = sqrt(3 * d*d / 4)+0.5;
	
	for (int i = 0; i < heightH; i++) {
		
		std::vector<Hexagon> h1;
		
		for (int j = 0; j < widhtH; j++)
		{
		
			h1.push_back(Hexagon(Point(x,y)));
			y += d*2;
		}
		
		this->MatHex.push_back(h1);
		
		x += 3* Hexagon::size/2;
		
		if (i % 2 == 0) {
			
			y = s + d;
		}
		else {
			
			y = s;
		}
	}
}


std::vector < std::vector<Hexagon>> Hive::getMatHex() {
	return this->MatHex;
}