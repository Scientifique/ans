#pragma once
#include <qdialog.h>

#include <QPushButton>

#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QColorDialog>
#include <QMessageBox>

#include "Antenna.h"

class AntenneDialog : public QDialog
{
	Q_OBJECT

private :
	Antenna *antenne;
	bool isColor;
	int x, y;

	QColor couleur;

	QGridLayout *layout;
	QLabel *LabName;
	QLabel *LabPuiss;
	QLabel *LabFreq;
	QLabel *LabPos;
	QLabel *LabX;
	QLabel *LabY;
	QLabel *LabColor;

	QLineEdit *txtName;
	QLineEdit *txtPuiss;
	QLineEdit *txtFreq;
	QLineEdit *txtPosX;
	QLineEdit *txtPosY;

	QPushButton *m_btnColor;
	QPushButton *m_btnConfirm;
public:
	Antenna *getAntenne();
	AntenneDialog(int x, int y);
	~AntenneDialog();
	

protected Q_SLOTS:
	void handleButton();
	void confirmButton();
	

};

