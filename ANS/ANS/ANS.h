#pragma once

#include <QtWidgets/QMainWindow>
#include <qgraphicsscene.h>
#include "View.h"
#include "Hexagon.h"
#include "Antenna.h"
#include "hive.h"
#include "ui_ANS.h"

#include "AntenneDialog.h"

#include <QColorDialog>
#include <QPushButton> 

class ANS : public QMainWindow
{
	Q_OBJECT

public:
	ANS(QWidget *parent = Q_NULLPTR);
	~ANS();
	void updateHexagons();
	void redrawView();
	void drawHexa(const Hexagon& hex);
	void drawHive();
	void drawAntennas();
	void mousePressEvent(QMouseEvent *event);
	void refresh();

private:
	Ui::ANSClass ui;

	Hive current;
	QGraphicsScene *myScene;
	View *view;
};
