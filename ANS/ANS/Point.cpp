#include "Point.h"



Point::Point(int x, int y): x{x}, y{y}
{
}


Point::Point(): x{0}, y{0}
{
}

int Point::X() const {
	return this->x;
}

int Point::Y() const {
	return this->y;
}

void Point::setX(int x) {
	this->x=x;
}

void Point::setY(int y) {
	this->y = y;
}
QVector<QPoint> Point::Convert(std::vector<Point> &tp) {
	QVector<QPoint> vec = QVector<QPoint>();
	
	for (int i = 0; i < 6; i++) {
		vec.append(QPoint(tp[i].X(), tp[i].Y()));
	}

	return vec;
}

double Point::Distance(Point& p) {
	
	return sqrt( (this->x - p.X() ) * (this->x - p.X()) + (this->y - p.Y()) * (this->y - p.Y()) );
}