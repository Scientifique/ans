#include "Color.h"



Color::Color(int r, int g, int b, int a): r(r), g(g), b(b), alpha(a)
{
}

Color::Color(int r, int g, int b) : r(r), g(g), b(b), alpha(255)
{
}

Color::Color(const Color& c, int a) : r(c.r), g(c.g), b(c.b), alpha(a)
{
}

Color::Color(const Color& c) : r(c.r), g(c.g), b(c.b), alpha(c.alpha)
{
}

Color Color::Synthesize(const std::vector<Color> tc) {

	
	if (tc.size() == 0) {
		return Color(0, 0, 0, 255);
	}

	int size = tc.size();
	
	int r=0, g=0, b=0;
	int alpha = 0;

	double totalSignal = 0;

	for (int i = 0; i < size; i++) {

		r += tc[i].r * tc[i].alpha;
		g += tc[i].g * tc[i].alpha;
		b += tc[i].b * tc[i].alpha;

		alpha = (alpha > tc[i].alpha)? alpha : tc[i].alpha;

		totalSignal += tc[i].alpha;
	}

	if (totalSignal == 0) {

		r = 224;
		g = 224;
		b = 224;
		alpha = 0;

	} else {
		r = r / (totalSignal);
		g = g / (totalSignal);
		b = b / (totalSignal);
	}

	return Color(r, g, b, alpha);
}

int Color::R() const {
	return this->r;
}

int Color::G() const {
	return this->g;
}

int Color::B() const {
	return this->b;
}

int Color::Alpha() const {
	return this->alpha;
}

void Color::setR(int r) {
	this->r = r;
}

void Color::setG(int g) {
	this->g = g;
}

void Color::setB(int b) {
	this->b = b;
}

void Color::setAlpha(int alpha) {
	this->alpha = alpha;
}