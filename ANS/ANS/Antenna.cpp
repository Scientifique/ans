#include "Antenna.h"
#include <string>
#include <qpen.h>
#include <qpainter.h>

using namespace std;

double Antenna::min = 4.0;
double Antenna::max = 40.0;
const double pi = 3.14159265358979323846;

Antenna::Antenna(double pow, int freq, string n, Color c, Point pos) : power{ pow }, frequency { freq }, name{ n }, color{ c }, p{pos}
{
	this->setPos(pos.X(), pos.Y());
	setFlag(QGraphicsItem::ItemIsSelectable);
	setFlag(QGraphicsItem::ItemIsMovable);
}

double Antenna::Signal(double distance)
{
	double signal = power / (4 * pi *(distance * distance));
	return signal;
}

double Antenna::GetInBounds(double signal)
{
	if (signal > Antenna::max)
	{
		return Antenna::max;
	}
	else if (signal < Antenna::min)
	{
		return Antenna::min;
	}
	return signal;
}

int Antenna::ToTransparency(double inboundSignal)
{
	inboundSignal -= Antenna::min;
	inboundSignal /= (Antenna::max - Antenna::min);
	int transparency = inboundSignal * 255;
	return transparency;
}

Color Antenna::GetColor() {

	return this->color;
}

Point Antenna::Position() {
	return this->p;
}

void Antenna::SetPosition(int x, int y) {

	p.setX(x);
	p.setY(y);
	setPos(p.X(), p.Y());
}

string Antenna::getName()
{
	return name;
}

QRectF Antenna::boundingRect() const {

	return QRectF(-15, -25, 30, 50);
}

void Antenna::paint(QPainter *painter, const QStyleOptionGraphicsItem * option, QWidget * widget) {

	QRectF rect = boundingRect();
	QPen pen(Qt::black, 3);

	painter->setPen(pen);

	QPointF p1, p2, p3, p4, p5, p6;
	p1 = rect.bottomLeft();
	p1 = (p1 * 5 + rect.center()) / 6;

	p2 = (rect.topLeft() + rect.topRight()) / 2;
	p2 = (p2 * 5 + rect.center()) / 6;

	p3 = rect.bottomRight();
	p3 = (p3 * 5 + rect.center()) / 6;

	p4 = (p1 * 2 + p2) / 3;

	p5 = (p2 + p3) / 2;

	p6 = (p1 + p2 * 2) / 3;

	QPainterPath path = QPainterPath(p1);
	
	path.lineTo(p2);
	path.lineTo(p3);
	path.lineTo(p4);
	path.lineTo(p5);
	path.lineTo(p6);

	QRectF r1 = QRectF(p1.x()-2, p1.y()-4, 6, 6);
	QRectF r2 = QRectF(p3.x()-4, p3.y()-4, 6, 6);
	painter->drawPath(path);
	painter->fillRect(r1, Qt::black);
	painter->fillRect(r2, Qt::black);
}

void Antenna::mousePressEvent(QGraphicsSceneMouseEvent *event) {
	
	QGraphicsItem::mousePressEvent(event);
}

void Antenna::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {

	if (event->button() == Qt::LeftButton) {
		SetPosition(event->scenePos().x(), event->scenePos().y());
	}
	QGraphicsItem::mouseReleaseEvent(event);
}
