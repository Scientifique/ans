#pragma once
#include "Point.h"
#include "Color.h"
#include <string>
#include <qgraphicsitem.h>
#include <qgraphicssceneevent.h>
#include <qgraphicsscene.h>

using namespace std;

class Antenna : public QGraphicsItem{

private:
	
	static double min;
	static double max;
	double power;
	int frequency;
	string name;
	Point p;
	Color color;

public:

	Antenna(double pow, int freq, string n, Color c, Point pos);

	double Signal(double distance);
	static double GetInBounds(double signal);
	static int ToTransparency(double inboundSignal);

	Color GetColor();
	Point Position();
	void SetPosition(int x, int y);
	string getName();

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
};

