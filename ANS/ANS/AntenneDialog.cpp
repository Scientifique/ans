#include "AntenneDialog.h"




AntenneDialog::AntenneDialog(int x, int y)
{
	this->x = x;
	this->y = y;
	isColor = 0;
	antenne = NULL;
	setFixedSize(500, 200);
	setWindowTitle("Ajouter une Antenne");
	LabName = new QLabel(tr("Nom de l'antenne:"));
	LabPuiss = new QLabel(tr("Puissance de l'antenne:"));
	LabFreq = new QLabel(tr("Frequence de l'antenne:"));
	//LabPos = new QLabel(tr("Position de l'antenne:"));
	//LabX = new QLabel(tr("position de X:"));
	//LabY = new QLabel(tr("position de Y:"));
	LabColor = new QLabel(tr("Couleur de l'antenne:"));

	txtName = new QLineEdit;
	txtPuiss = new QLineEdit;
	txtFreq = new QLineEdit;
	//txtPosX = new QLineEdit;
	//txtPosY = new QLineEdit;

	txtName->setFixedSize(100, 20);
	txtPuiss->setFixedSize(100,20);
	txtFreq->setFixedSize(100, 20);
	//txtPosX->setFixedSize(80, 20);
	//txtPosY->setFixedSize(80, 20);
	
	m_btnColor = new QPushButton("choisir", this);
	QObject::connect(m_btnColor, SIGNAL(clicked()), this, SLOT(handleButton()));
	
	m_btnConfirm = new QPushButton("Confirmer",this);
	QObject::connect(m_btnConfirm, SIGNAL(clicked()), this, SLOT(confirmButton()));

	layout = new QGridLayout();

	layout->addWidget(LabName, 0, 0, 1, 1);
	layout->addWidget(txtName, 0, 1, 1, 4);
	
	layout->addWidget(LabPuiss, 1, 0,1,1);
	layout->addWidget(txtPuiss, 1, 1,1,4);
	
	layout->addWidget(LabFreq, 2, 0, 1, 1);
	layout->addWidget(txtFreq, 2, 1, 1, 4);
	
	//layout->addWidget(LabPos, 3, 0, 1, 1);
	//layout->addWidget(LabX, 3, 1, 1, 1);
	//layout->addWidget(txtPosX, 3, 2, 1, 1);
	//layout->addWidget(LabY, 4, 1, 1, 1);
	//layout->addWidget(txtPosY, 4, 2, 1, 1);
	
	layout->addWidget(LabColor, 5, 0, 1, 1);
	layout->addWidget(m_btnColor, 5, 1, 1, 1);
	
	layout->addWidget(m_btnConfirm, 6, 1);
	
	setLayout(layout);
}
void AntenneDialog::handleButton() {
	 couleur = QColorDialog::getColor(Qt::white,this);
	 isColor = true;
}

void AntenneDialog::confirmButton() {
	QString nom = txtName->text();
	QString puissance = txtPuiss->text();
	QString frequence = txtFreq->text();
	//QString X = txtPosX->text();
	//QString Y = txtPosY->text();
	
	if (nom.isEmpty() || puissance.isEmpty() || frequence.isEmpty()) { //|| X.isEmpty() || Y.isEmpty()) {
		
		QMessageBox::information(this, "informations manquantes", "veuillez remplir tous les champs");
	
	} else if (isColor == false) {

		QMessageBox::information(this, "informations manquantes", "Veuillez choisir une couleur");

	} else {

		//int x = X.toInt();
		//int y = Y.toInt();
		antenne = new Antenna(puissance.toInt(), frequence.toInt(), nom.toStdString(), Color(couleur.red(), couleur.green(), couleur.blue()), Point(this->x, this->y));

		this->close();
	}
}



Antenna* AntenneDialog::getAntenne()
{
	return antenne;
}


AntenneDialog::~AntenneDialog()
{
	delete this;
}
