#include "View.h"

View::View()
{

	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	this->setDragMode(ScrollHandDrag);
	this->setMouseTracking(true);
	
	AddAnt = new QAction("&Ajouter une antenne", this);
	AddMap = new QAction("&Ajouter un fond de carte", this);

	menu = new QMenu("Fichier", this);
	menu->addAction(AddAnt);
	menu->addAction(AddMap);
	//AddAnt->setShortcut(tr("Ctrl+N"));
	connect(AddAnt, SIGNAL(triggered()),this, SLOT(AjouterAntenne()));	
	connect(AddMap, SIGNAL(triggered()), this, SLOT(AjouterCarte()));
}


View::~View()
{
}

void View::setAntennaNetwork(std::vector<Antenna*> *network) {

}

void View::wheelEvent(QWheelEvent *event) {

	double step = 1.0;
	if (event->delta() > 0) {

		step = 1.25;
	}
	else if (event->delta() < 0) {

		step = 0.8;
	}

	scale(step, step);
}

void View::mousePressEvent(QMouseEvent *event) {

	if (event->button() == Qt::RightButton) {

		//menu.addAction(copyAct);
		//menu.addAction(pasteAct);

		QPoint point = event->pos();
		QPointF pos = mapToScene(point);

		m_originX = pos.x();
		m_originY = pos.y();

		menu->exec(event->globalPos());

	} else {

		QGraphicsView::mousePressEvent(event);
	}
}

void View::mouseReleaseEvent(QMouseEvent *event) {

	QGraphicsView::mouseReleaseEvent(event);
}

void View::AjouterAntenne() {

	AntenneDialog *log = new AntenneDialog(this->m_originX, this->m_originY);
	if (log->exec() == 0) {
		if (log->getAntenne() != NULL){

			network.push_back(log->getAntenne());
			QMessageBox::information(this, "Antenne ajoutee", "Votre Antenne " + QString::fromStdString(log->getAntenne()->getName())+ " a ete bien ajoutee");
		}
	}
}

void View::AjouterCarte()
{
	QString fileName = QFileDialog::getOpenFileName(
		this,
		"Choisissez une image:",
		QDir::currentPath(),
		"All files (*.*) ;; PNG files (.png)"
	);

	if (!fileName.isNull()) {


	}
}
