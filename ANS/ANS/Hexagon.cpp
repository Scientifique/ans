
#include "Hexagon.h"
#include <math.h>

int Hexagon::size = 40;

Hexagon::Hexagon(const Point& p): p{p}, c{Color(0,0,0,64)}
{
}

void Hexagon::setColor(const Color& col) {

	this->c.setR(col.R());
	this->c.setG(col.G());
	this->c.setB(col.B());

	this->c.setAlpha(col.Alpha());
}

void Hexagon::setSize(int s) {
	Hexagon::size = s;
}

Color Hexagon::getColor() const {
	return this->c;
}

Point Hexagon::Position() {
	return this->p;
}

std::vector<Point> Hexagon::getVertices() const {
	
	std::vector<Point> tp;
	const double pi = 3.14159265358979323846;

	for (int i = 0; i < 6; i++) {

		Point v = Point{ static_cast<int>(p.X()*1.0 + size * cos((i*pi) / 3.0)),
						 static_cast<int>(p.Y()*1.0 + size * sin((i*pi) / 3.0)) };

		tp.push_back(v);
	}

	return tp;
}