#pragma once

#include <vector>

class Color
{

private:

	int r; // red value of the pixel
	int g; // green value of the pixel
	int b; // blue value of the pixel

	int alpha; // transparency value of the pixel
	// all 4 values are between 0 and 255

public:

	Color(int r, int g, int b, int a);
	Color(int r, int g, int b);
	Color(const Color& c, int a);
	Color(const Color& c);

	static Color Synthesize(const std::vector<Color> tc);
	
	int R()const;
	int G()const;
	int B()const;
	int Alpha()const;

	void setR(int r);
	void setG(int g);
	void setB(int b);
	void setAlpha(int alpha);

};

